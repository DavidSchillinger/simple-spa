A simple SPA consuming a Node.js API.
This SPA will:

1. Display a list of users including their ID, name, username and email.
2. Display a list of posts for a user clicked and page 1. allowing the user to navigate back to page 1.
3. Display a list of comments for a specific post clicked in page 2. allowing the user to navigate back to 2.