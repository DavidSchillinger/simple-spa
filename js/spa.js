/* global angular */
/* No ES6 ARROW FUNCTION SUPPORT IN ANGULAR YET */

var myApp = angular.module('myApp', ['ngRoute'])

// Some global variables to store information across different routes.
var users = ''
var posts = ''
var userTempId = ''

myApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/users', {
        templateUrl: 'templates/users.html',
        controller: 'usersController'
    })
    .when('/posts/:userId', {
        templateUrl: 'templates/posts.html',
        controller: 'postsController'
    })
    .when('/comments/:postId', {
        templateUrl: 'templates/comments.html',
        controller: 'commentsController'
    })
    .otherwise({
        redirectTo: 'users'
    })
}])

// This controller fires up whenever the default /users route is connected to, it retrieves the current list of users from the API and displays it to the viewer.
myApp.controller('usersController', function($scope, $http) {
    $scope.message = 'See all current users below:'
    
    var headers = {'Authorization': 'Basic c2ltcGxlOndlYmRldg==', 'Accept': 'application/json'}
    var url = 'https://simple-api-davidsch.c9users.io/users'
    var method = 'GET'

    $http({method:method, url:url, headers:headers})
    .then (function(response) {
        $scope.users = response.data
        users = response.data
    })
})

/* Similar in functionality, this controller takes the userId provided through the link in order to retrieve posts. 
It also uses the global users information generated when accessing the first page in order to display the users name.*/
myApp.controller('postsController', function($scope, $routeParams, $http) {
    $scope.message = ''
    $scope.name = "PostsController"
    $scope.params = $routeParams
    userTempId = $scope.params.userId
    
    var headers = {'Authorization': 'Basic c2ltcGxlOndlYmRldg==', 'Accept': 'application/json'}
    var url = 'https://simple-api-davidsch.c9users.io/posts?userId='+$scope.params.userId
    var method = 'GET'
    
    function findUser(userId) {
        return users.find(function(user) {
            return user.id === userId
        })
    }
    
    $scope.userName = findUser($scope.params.userId).username
    
    $http({method:method, url:url, headers:headers})
    .then (function(response) {
        $scope.posts = response.data
        posts = response.data
    })
})

// Comments are currently listed by their IDs I believe it would be better to have the userId assigned to them in the database scheme.
myApp.controller('commentsController', function($scope, $routeParams, $http) {
    $scope.message = ''
    $scope.userId = userTempId
    $scope.name = "CommentsController"
    $scope.params = $routeParams
    
    var headers = {'Authorization': 'Basic c2ltcGxlOndlYmRldg==', 'Accept': 'application/json'}
    var url = 'https://simple-api-davidsch.c9users.io/comments?postId='+$scope.params.postId
    var method = 'GET'
    
    function findPost(postId) {
        return posts.find(function(post) {
            return post.id === postId
        })
    }
    
    $scope.postTitle = findPost($scope.params.postId).title
    
    $http({method:method, url:url, headers:headers})
    .then (function(response) {
        $scope.comments = response.data
    })
})